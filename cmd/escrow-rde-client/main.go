/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"

	wf "github.com/danielb42/whiteflag"
	"github.com/sebidude/configparser"
	"gitlab.com/team-escrow/escrow-rde-client/config"
	"gitlab.com/team-escrow/escrow-rde-client/internal/logging"
	"gitlab.com/team-escrow/escrow-rde-client/processor"
)

const appname = "escrow-rde-client"

var (
	buildtime  string
	gitcommit  string
	appversion string

	printversion = false
	initconfig   = false
	silent       = false

	configfile = "config.yaml"

	workdir string

	appconfig config.AppConfig
)

func cleanup() {
	if r := recover(); r != nil {
		fmt.Println()
		fmt.Println("###################")
		fmt.Printf("Fatal error: %v\n", r)
	}
}

func main() {
	defer cleanup()

	readCommandLine()

	if initconfig {
		createConfigFile()
		os.Exit(0)
	}

	if printversion {
		fmt.Printf("Builddate: %s\n", buildtime)
		fmt.Printf("Version  : %s\n", appversion)
		fmt.Printf("Revision : %s\n", gitcommit)
		fmt.Println(licenseNotice)
		os.Exit(0)
	}

	// parse the config
	err := configparser.ParseYaml(configfile, &appconfig)
	if err != nil {
		log.Fatalf("error parsing config: %v", err.Error())
	}

	log.SetFlags(0)
	logger := new(logging.Logger)

	if len(appconfig.LogFile) > 0 {
		logfile, err := os.OpenFile(appconfig.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0660)
		if err != nil {
			log.Fatalf("error opening logfile: %v", err.Error())
		}
		defer logfile.Close()
		logger.Destination = logfile

	} else {
		appconfig.LogFile = "stdout"
		logger.Destination = os.Stdout
	}

	p, err := processor.New(appname, logger, appconfig, configfile, silent)
	if err != nil {
		log.Fatalf("Error creating processor: %v", err.Error())
	}
	destinationDir, err := p.Process()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Process terminated successfully. Destination files are in %q dir\n", destinationDir)
}

func readCommandLine() {
	wf.Alias("i", "init", "Create an example configfile. If the file already exists it will be overwritten.")
	wf.Alias("c", "config", "Location of config file")
	wf.Alias("v", "version", "Print version and licensing information and exit")
	wf.Alias("s", "silent", "Do not print validation results on the command line")

	wf.ParseCommandLine()

	initconfig = wf.CheckBool("init")
	silent = wf.CheckBool("silent")
	printversion = wf.CheckBool("version")

	if wf.CheckString("config") {
		configfile = wf.GetString("config")
	}
}
