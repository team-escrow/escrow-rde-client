/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package config

type GpgConfig struct {
	GpgPrivateKeyPath     string `json:"gpgPrivateKeyPath"`
	GpgPrivateKeyPass     string `json:"gpgPrivateKeyPass"`
	GpgReceiverPubKeyPath string `json:"gpgReceiverPubKeyPath"`
}

type SshConfig struct {
	SshPrivateKeyPath     string `json:"sshPrivateKeyPath"`
	SshPrivateKeyPassword string `json:"sshPrivateKeyPassword"`
	SshHostPublicKeyPath  string `json:"sshHostPublicKeyPath"`
	SshHostname           string `json:"sshHostname"`
	SshPort               int    `json:"sshPort"`
	SshUsername           string `json:"sshUsername"`
}
type AppConfig struct {
	IanaID             string     `json:"ianaID"`
	Specification      string     `json:"specification"`
	DepositBaseDir     string     `json:"depositBaseDir"`
	RunDir             string     `json:"runDir"`
	UploadFiles        bool       `json:"uploadFiles"`
	Multi              bool       `json:"multi"`
	CompressAndEncrypt bool       `json:"compressAndEncrypt"`
	GpgConfig          *GpgConfig `json:"gpg"`
	SshConfig          *SshConfig `json:"sftp"`
	LogFile            string     `json:"logFile"`
	SkipValidation     bool       `json:"skipValidation"`
	UseFileSystemCache bool       `json:"useFileSystemCache"`
}
