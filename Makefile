#
# Copyright (C) 2018 DENIC eG
#
# This file is part of escrow-rde-client.
#
#	escrow-rde-client is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Lesser General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	escrow-rde-client is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Lesser General Public License for more details.
#
#	You should have received a copy of the GNU Lesser General Public License
#	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.


BASEDIR := $(shell pwd)
APPNAME := escrow-rde-client
APPSRC := cmd/$(APPNAME)/*

GITCOMMITHASH := $(shell git log --max-count=1 --pretty="format:%h" HEAD)
GITCOMMIT := -X main.gitcommit=$(GITCOMMITHASH)

VERSIONTAG := $(shell cat release.config)
VERSION := -X main.appversion=$(VERSIONTAG)

BUILDTIMEVALUE := $(shell date +%Y-%m-%dT%H:%M:%S%z)
BUILDTIME := -X main.buildtime=$(BUILDTIMEVALUE)

LDFLAGS := '-extldflags "-static" -d -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
LDFLAGS_WINDOWS := '-extldflags "-static" -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
all: dep clean build testall

info: 
	@echo - appname:   $(APPNAME)
	@echo - version:   $(VERSIONTAG)
	@echo - commit:    $(GITCOMMIT)
	@echo - buildtime: $(BUILDTIME) 
dep:
	@go get -v -d ./...

unittest:
	@go test -race ./...

build: info build-linux build-windows build-darwin

build-linux: info linux-x86_64

build-windows: info windows

build-darwin: info darwin

darwin: info dep
	@echo Building for darwin
	@mkdir -p build/darwin
	@CGO_ENABLED=0 \
	GOOS=darwin \
	go build -o build/darwin/$(APPNAME)-$(VERSIONTAG)-darwin -a -ldflags $(LDFLAGS_WINDOWS) $(APPSRC)
	@cd $(BASEDIR)/build/darwin && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-darwin > $(APPNAME)-$(VERSIONTAG)-darwin.sha256sum

windows: info dep
	@echo Building for windows
	@mkdir -p build/windows
	@CGO_ENABLED=0 \
	GOOS=windows \
	go build -o build/windows/$(APPNAME)-$(VERSIONTAG).exe -a -ldflags $(LDFLAGS_WINDOWS) $(APPSRC)
	@cd $(BASEDIR)/build/windows && \
	sha256sum $(APPNAME)-$(VERSIONTAG).exe > $(APPNAME)-$(VERSIONTAG).exe.sha256sum

linux-x86_64: info dep
	@echo Building for linux
	@mkdir -p build/linux
	@CGO_ENABLED=0 \
	GOOS=linux \
	go build -o build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64 -a -ldflags $(LDFLAGS) $(APPSRC)
	@cd $(BASEDIR)/build/linux && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-linux_x86_64 > $(APPNAME)-$(VERSIONTAG)-linux_x86_64.sha256sum

clean:
	@echo "Cleaning up"
	@rm -rf build
	@rm -rf deposit-run*
	@rm -rf release

testnoupload:
	@echo "running test without upload"
	./build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64 -i
	./build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64 -c config-rde-client-example-$(VERSIONTAG).yaml

testnoupload-darwin:
	@echo "running test without upload on darwin"
	./build/darwin/$(APPNAME)-$(VERSIONTAG) -i
	./build/darwin/$(APPNAME)-$(VERSIONTAG) -c config-rde-client-example-$(VERSIONTAG).yaml

testall:
	@echo "running test with upload"
	./build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64 -c examples/configs/config-test.yaml

bintest: build
	@echo "running binary test"
	./build/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64 -v

release-linux:
	@mkdir -p release/linux
	@echo Bundle linux release $(VERSIONTAG)
	@echo -n "verify checksum: " 
	@cd build/linux && \
	sha256sum -c $(APPNAME)-$(VERSIONTAG)-linux_x86_64.sha256sum
	
	@cd build/linux && \
	tar cfz ../../release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz $(APPNAME)-$(VERSIONTAG)-linux_x86_64
	@cd release/linux && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz > $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz.sha256sum
	@echo archive created: $(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz

release-windows:
	@mkdir -p release/windows
	@echo Bundle windows release $(VERSIONTAG)
	@echo -n "verify checksum: " 
	@cd build/windows && \
	sha256sum -c $(APPNAME)-$(VERSIONTAG).exe.sha256sum
	
	@echo adding zip package
	@apt-get update && apt-get install -y zip

	@cd build/windows && \
	zip ../../release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip $(APPNAME)-$(VERSIONTAG).exe
	@cd release/windows && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-windows.zip > $(APPNAME)-$(VERSIONTAG)-windows.zip.sha256sum
	@echo archive created: $(APPNAME)-$(VERSIONTAG)-windows.zip

release-darwin:
	@mkdir -p release/darwin
	@echo Bundle darwin release $(VERSIONTAG)
	@echo -n "verify checksum: " 
	@cd build/darwin && \
	sha256sum -c $(APPNAME)-$(VERSIONTAG)-darwin.sha256sum
	
	@cd build/darwin && \
	tar cfz ../../release/darwin/$(APPNAME)-$(VERSIONTAG)-darwin.tar.gz $(APPNAME)-$(VERSIONTAG)-darwin
	@cd release/darwin && \
	sha256sum $(APPNAME)-$(VERSIONTAG)-darwin.tar.gz > $(APPNAME)-$(VERSIONTAG)-darwin.tar.gz.sha256sum
	@echo archive created: $(APPNAME)-$(VERSIONTAG)-darwin.tar.gz

update-releasepage:
	apt update && apt install markdown
	mkdir -p public/releases
	git archive HEAD | gzip > public/releases/$(APPNAME)-$(VERSIONTAG).src.tar.gz
	@cd public/releases && \
	sha256sum $(APPNAME)-$(VERSIONTAG).src.tar.gz > $(APPNAME)-$(VERSIONTAG).src.tar.gz.sha256sum
	cp release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz public/releases
	cp release/linux/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz.sha256sum public/releases
	cp release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip public/releases
	cp release/windows/$(APPNAME)-$(VERSIONTAG)-windows.zip.sha256sum public/releases
	cp release/darwin/$(APPNAME)-$(VERSIONTAG)-darwin.tar.gz public/releases
	cp release/darwin/$(APPNAME)-$(VERSIONTAG)-darwin.tar.gz.sha256sum public/releases
	markdown page/index-body.md > page/index-body.tpl
	cat page/index-header.tpl page/index-body.tpl page/index-footer.tpl > public/index.html 
	sed -i 's/__release_linux__/$(APPNAME)-$(VERSIONTAG)-linux_x86_64.tar.gz/g' public/index.html
	sed -i 's/__release_windows__/$(APPNAME)-$(VERSIONTAG)-windows.zip/g' public/index.html
	sed -i 's/__release_darwin__/$(APPNAME)-$(VERSIONTAG)-darwin.tar.gz/g' public/index.html
	sed -i 's/__release_source__/$(APPNAME)-$(VERSIONTAG).src.tar.gz/g' public/index.html
