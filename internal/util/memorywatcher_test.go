package util

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMemoryWatcher(t *testing.T) {
	watcher := NewMemoryWatcher(1 * time.Millisecond)
	_ = watcher.Start()

	dummy := []int{}
	for i := 0; i < 10000; i++ {
		dummy = append(dummy, i)
		if i%1000 == 0 {
			t.Log("Current Memory: ", watcher.FormatPeakMemoryUsage())
		}
	}

	assert.Equal(t, watcher.IsActive(), true)

	period, _ := watcher.Stop()
	t.Log("Peak Memory Usage: ", watcher.FormatPeakMemoryUsage())
	t.Log("Process Duration:       ", period)
	assert.Equal(t, watcher.IsActive(), false)

	// Using "-race" option on test execution should be ok
}
