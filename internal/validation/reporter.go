/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Reporter represents the basic functionality for validation message collection.
type Reporter interface {
	ReportInfo(msg string)
	ReportError(err error)
	GetInfoCount() int
	GetErrorCount() int
	HasErrors() bool
	Close()
}

// HistoryReporter is an extension to Reporter that is able to output all messages to the log.
type HistoryReporter interface {
	Reporter
	WriteToLog() error
}

func putInfo(reporter Reporter, format string, a ...interface{}) {
	if reporter != nil {
		reporter.ReportInfo(fmt.Sprintf(format, a...))
	}
}

func putError(reporter Reporter, err error) {
	if reporter != nil {
		reporter.ReportError(err)
	}
}

// SilentReporter only stores the last error message and the number of validation messages.
type SilentReporter struct {
	infoCount  int
	errorCount int
	lastError  error
}

// ReportInfo stores a generic validation information.
func (reporter *SilentReporter) ReportInfo(msg string) {
	reporter.infoCount++
}

// ReportError stores a validation error.
func (reporter *SilentReporter) ReportError(err error) {
	reporter.errorCount++
	reporter.lastError = err
}

// GetInfoCount returns the number of generic validation information messages.
func (reporter *SilentReporter) GetInfoCount() int {
	return reporter.infoCount
}

// GetErrorCount returns the number of validation errors.
func (reporter *SilentReporter) GetErrorCount() int {
	return reporter.errorCount
}

// HasErrors returns true when the reporter has captured at least one validation error.
func (reporter *SilentReporter) HasErrors() bool {
	return reporter.errorCount > 0
}

// GetLastError returns the last validation error that was stored.
func (reporter *SilentReporter) GetLastError() error {
	return reporter.lastError
}

// Close releases all resources.
func (reporter *SilentReporter) Close() {}

// NewSilentReporter returns a new instance of SilentReporter.
func NewSilentReporter() Reporter {
	return &SilentReporter{0, 0, nil}
}

// MemoryReporter is a HistoryReporter that stores all validation messages in memory.
type MemoryReporter struct {
	infos  []string
	errors []string
}

// ReportInfo stores a generic validation information.
func (reporter *MemoryReporter) ReportInfo(msg string) {
	reporter.infos = append(reporter.infos, msg)
}

// ReportError stores a validation error.
func (reporter *MemoryReporter) ReportError(err error) {
	reporter.errors = append(reporter.errors, err.Error())
}

// GetInfoCount returns the number of generic validation information messages.
func (reporter *MemoryReporter) GetInfoCount() int {
	return len(reporter.infos)
}

// GetErrorCount returns the number of validation errors.
func (reporter *MemoryReporter) GetErrorCount() int {
	return len(reporter.errors)
}

// HasErrors returns true when the reporter has captured at least one validation error.
func (reporter *MemoryReporter) HasErrors() bool {
	return len(reporter.errors) > 0
}

// WriteToLog outputs all validation informations and errors to the current log.
func (reporter *MemoryReporter) WriteToLog() error {
	for _, msg := range reporter.infos {
		log.Println(msg)
	}
	for _, msg := range reporter.errors {
		log.Println(msg)
	}
	return nil
}

// Close releases all resources.
func (reporter *MemoryReporter) Close() {}

// NewMemoryReporter returns a new instance of MemoryReporter.
func NewMemoryReporter() HistoryReporter {
	return &MemoryReporter{[]string{}, []string{}}
}

// FileSystemReporter is a HistoryReporter that stores all validation messages on the local file-system.
type FileSystemReporter struct {
	infoCount  int
	errorCount int
	infoFile   *os.File
	errorFile  *os.File
}

// ReportInfo stores a generic validation information.
func (reporter *FileSystemReporter) ReportInfo(msg string) {
	reporter.infoCount++
	reporter.infoFile.WriteString(msg + "\n")
}

// ReportError stores a validation error.
func (reporter *FileSystemReporter) ReportError(err error) {
	reporter.errorCount++
	reporter.errorFile.WriteString(err.Error() + "\n")
}

// GetInfoCount returns the number of generic validation information messages.
func (reporter *FileSystemReporter) GetInfoCount() int {
	return reporter.infoCount
}

// GetErrorCount returns the number of validation errors.
func (reporter *FileSystemReporter) GetErrorCount() int {
	return reporter.errorCount
}

// HasErrors returns true when the reporter has captured at least one validation error.
func (reporter *FileSystemReporter) HasErrors() bool {
	return reporter.errorCount > 0
}

// WriteToLog outputs all validation informations and errors to the current log.
func (reporter *FileSystemReporter) WriteToLog() error {
	if err := printToLog(reporter.infoFile); err != nil {
		return err
	}
	if err := printToLog(reporter.errorFile); err != nil {
		return err
	}
	return nil
}
func printToLog(file *os.File) error {
	if _, err := file.Seek(0, 0); err != nil {
		return err
	}
	reader := bufio.NewReader(file)
	var sb strings.Builder
	for {
		line, isPrefix, err := reader.ReadLine()
		fmt.Printf("%v | %v | %v", line, isPrefix, err)
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		sb.Write(line)
		if !isPrefix {
			log.Println(sb.String())
			sb.Reset()
		}
	}
	return nil
}

// Close deletes all cache files.
func (reporter *FileSystemReporter) Close() {
	reporter.infoFile.Close()
	reporter.errorFile.Close()
	os.Remove(reporter.infoFile.Name())
	os.Remove(reporter.errorFile.Name())
}

// NewFileSystemReporter creates temporary files for validation message caching.
func NewFileSystemReporter() (HistoryReporter, error) {
	infoFile, err := ioutil.TempFile("", "rde-client-infolog")
	if err != nil {
		return nil, err
	}
	errorFile, err := ioutil.TempFile("", "rde-client-errorlog")
	if err != nil {
		infoFile.Close()
		os.Remove(infoFile.Name())
		return nil, err
	}
	return &FileSystemReporter{0, 0, infoFile, errorFile}, nil
}
