/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSilentReporter(t *testing.T) {
	reporter, _ := NewSilentReporter().(*SilentReporter)
	defer reporter.Close()
	assert.Equal(t, nil, reporter.GetLastError())
	testReporter(t, reporter)
	assert.Equal(t, errors.New("test error"), reporter.GetLastError())
	reporter.ReportError(errors.New("another error"))
	assert.Equal(t, errors.New("another error"), reporter.GetLastError())
}

func TestMemoryReporter(t *testing.T) {
	reporter := NewMemoryReporter()
	defer reporter.Close()
	testReporter(t, reporter)
}

func TestFileSystemReporter(t *testing.T) {
	reporter, err := NewFileSystemReporter()
	if err != nil {
		panic(err)
	}
	defer reporter.Close()
	testReporter(t, reporter)
}

func testReporter(t *testing.T, reporter Reporter) {
	assert.Equal(t, 0, reporter.GetInfoCount())
	assert.Equal(t, 0, reporter.GetErrorCount())
	assert.Equal(t, false, reporter.HasErrors())
	reporter.ReportInfo("foobar")
	assert.Equal(t, 1, reporter.GetInfoCount())
	assert.Equal(t, 0, reporter.GetErrorCount())
	assert.Equal(t, false, reporter.HasErrors())
	reporter.ReportError(errors.New("test error"))
	assert.Equal(t, 1, reporter.GetInfoCount())
	assert.Equal(t, 1, reporter.GetErrorCount())
	assert.Equal(t, true, reporter.HasErrors())
	reporter.ReportInfo("another information")
	assert.Equal(t, 2, reporter.GetInfoCount())
	assert.Equal(t, 1, reporter.GetErrorCount())
	assert.Equal(t, true, reporter.HasErrors())
}

func TestMemoryReporterPersistence(t *testing.T) {
	reporter := NewMemoryReporter()
	defer reporter.Close()
	testHistoryReporter(t, reporter)
}

func TestFileSystemReporterPersistence(t *testing.T) {
	reporter, err := NewFileSystemReporter()
	if err != nil {
		panic(err)
	}
	defer reporter.Close()
	testHistoryReporter(t, reporter)
}

func testHistoryReporter(t *testing.T, reporter HistoryReporter) {
	testEmptyHistory(t, reporter)
	testInterleavedHistory(t, reporter)
}

func testEmptyHistory(t *testing.T, reporter HistoryReporter) {
	buffer := new(bytes.Buffer)
	log.SetOutput(buffer)
	if err := reporter.WriteToLog(); err != nil {
		panic(err)
	}
	assert.Equal(t, "", buffer.String())
}

func testInterleavedHistory(t *testing.T, reporter HistoryReporter) {
	buffer := new(bytes.Buffer)
	log.SetOutput(buffer)
	reporter.ReportInfo("info 1")
	reporter.ReportError(errors.New("error 1"))
	reporter.ReportInfo("info 2")
	reporter.ReportError(errors.New("error 2"))
	reporter.ReportInfo("info 3")
	if err := reporter.WriteToLog(); err != nil {
		panic(err)
	}

	str := buffer.String()
	posInfo1 := strings.Index(str, "info 1")
	posInfo2 := strings.Index(str, "info 2")
	posInfo3 := strings.Index(str, "info 3")
	posError1 := strings.Index(str, "error 1")
	posError2 := strings.Index(str, "error 2")

	assertIsGreater(t, -1, posInfo1)
	assertIsGreater(t, posInfo1, posInfo2)
	assertIsGreater(t, posInfo2, posInfo3)
	assertIsGreater(t, posInfo3, posError1)
	assertIsGreater(t, posError1, posError2)
}

func assertIsGreater(t *testing.T, val1 int, val2 int) bool {
	if val1 >= val2 {
		assert.Fail(t, fmt.Sprintf("NOT: %v < %v", val1, val2))
		return false
	}
	return true
}
